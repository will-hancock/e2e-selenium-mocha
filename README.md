﻿
# Automate e2e testing with selenium, webdriverJS and mocha 

### Technical contacts
* Will Hancock - Web Developer - [will.hancock007@gmail.com](mailto:will.hancock007@gmail.com)

## Setup and Development
### System Requirements
Install the following;

* Environment: [Node.js](http://nodejs.org/)


### Build instructions
In your terminal;

* Checkout the repo ```git clone https://bitbucket.org/will-hancock/e2e-selenium-mocha``` to your desired directory.
* Navigate to inside the repo in your terminal
* Install the npm modules ```npm install```
* Run; ```jasmine-node-reporter-fix spec/``` 