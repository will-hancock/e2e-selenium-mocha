﻿var assert		= require('assert'),
	test		= require('selenium-webdriver/testing'),
	webdriver	= require('selenium-webdriver');


var driver = new webdriver.Builder().
    withCapabilities(webdriver.Capabilities.firefox()).
    build();

test.describe('basic test', function () {

	test.it('should be on correct page', function (done) {


		driver.get("http://www.google.com");
		driver.findElement(webdriver.By.name("q")).sendKeys("webdriver");
		driver.findElement(webdriver.By.name("btnG")).click();
		
		//driver.getTitle().then(function (title) {
		//	assert.equal(title, 'webdriver - Google Search', 'Title matches');
		//});

		driver.wait(function () {
			return driver.getTitle().then(function (title) {
				return title === 'webdriver - Google Search';
			});
		}, 5000);

	});
});